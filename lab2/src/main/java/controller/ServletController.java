package controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class ServletController {
    
    @GetMapping(value = "/hello")
    public String hello() {
        return "hello";
    }
}
