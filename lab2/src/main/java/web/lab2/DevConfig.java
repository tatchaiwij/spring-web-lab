package web.lab2;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({ "controller", "web.lab2" })

public class DevConfig{
    
}
