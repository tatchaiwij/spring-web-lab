package web.lab6;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class EmployeeController {
   
    @GetMapping(value = "/employees", produces = MediaType.APPLICATION_JSON_VALUE)
    public String getAllEmployeesJSON(Model model) {
        model.addAttribute("employees", getEmployeesCollection());
        return "jsonView";
    }
    private List<Employee> getEmployeesCollection() {
        List<Employee> employees = new ArrayList<>();
        Employee max = new Employee(1,"Pakawat","Boonyoung","max@gmail.com");
        Employee mee = new Employee(2,"Mee","Boonyoung","mee@yahoo.com");
        Employee panda = new Employee(3,"Panda","Boonyoung","panda@gmail.com");
        employees.add(max);
        employees.add(mee);
        employees.add(panda);
        return employees;
    }
}
