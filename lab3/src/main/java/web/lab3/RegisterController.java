package web.lab3;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class RegisterController {
    
    @GetMapping(value="/student")
    public ModelAndView student() {
        return new ModelAndView("student", "student", new Student());
    }

    @PostMapping(value="/register")
    public String addStudent(@ModelAttribute("student") Student student, ModelMap model) {
        model.addAttribute("name", student.getName());
        model.addAttribute("age", student.getAge());
        model.addAttribute("id", student.getId());
        return "studentResult";
    }
    
}
