package web.lab3;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@Configuration
@EnableWebMvc
@ComponentScan("web.lab3")
@PropertySource("classpath:application.properties")
public class DevConfig implements WebMvcConfigurer{

    @Bean
    public InternalResourceViewResolver templateResolver() {
        InternalResourceViewResolver Resolver = new InternalResourceViewResolver();
        Resolver.setPrefix("/WEB-INF/views/");
        Resolver.setSuffix(".jsp");  
        return Resolver;
    }

}
