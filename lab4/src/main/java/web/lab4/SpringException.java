package web.lab4;

public class SpringException extends RuntimeException{
    private static final long serialVersionUID = 1L;

    public SpringException(String message) {
        super(message);
    }
   
}
