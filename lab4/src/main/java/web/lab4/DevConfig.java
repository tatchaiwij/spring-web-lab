package web.lab4;

import java.util.Properties;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@Configuration
@EnableWebMvc
@ComponentScan("web.lab4")
@PropertySource("classpath:application.properties")
public class DevConfig implements WebMvcConfigurer{
    
    @Bean
    public HandlerExceptionResolver errorHandler () {
        SimpleMappingExceptionResolver s = new SimpleMappingExceptionResolver();
        Properties p = new Properties();
        p.setProperty(SpringException.class.getName(), "springExceptionView");
        s.setExceptionMappings(p);
        s.setDefaultStatusCode(400);
        s.setOrder(Ordered.HIGHEST_PRECEDENCE);
        return s;
    }
 
    @Bean
    public InternalResourceViewResolver templateResolver() {
        InternalResourceViewResolver Resolver = new InternalResourceViewResolver();
        Resolver.setPrefix("/WEB-INF/views/");
        Resolver.setSuffix(".jsp");  
        return Resolver;
    }

}
